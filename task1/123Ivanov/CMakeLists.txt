set(SRC_FILES
    gl1_SimpleTriangle.cpp
)

MAKE_OPENGL_TASK(123Ivanov 1 "${SRC_FILES}")

if (UNIX)
    target_link_libraries(123Ivanov1 stdc++fs)
endif()
