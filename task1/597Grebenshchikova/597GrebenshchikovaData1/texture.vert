#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalToCameraMatrix;


layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTexCoord;


out vec2 texCoord;
out vec3 normalCamSpace;
out vec4 posCamSpace;


void main()
{
	posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
	normalCamSpace = normalize(normalToCameraMatrix * vertexNormal);


	texCoord = vertexTexCoord;


	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}