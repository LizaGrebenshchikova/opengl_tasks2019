#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/CrossCapSurface.hpp"
#include "common/Texture.hpp"
#include "common/LightInfo.hpp"

#include <iostream>

class CrossCapSurfaceApplication : public Application
{
public:
    ShaderProgramPtr _shader;
    
    void makeScene() override
    {
        Application::makeScene();
        
        _crossCapSurface = makeCrossCapSurface(1.0f, _detalization, _shift);
        _crossCapSurface->setModelMatrix(glm::mat4(1.0f));

        _cameraMover = std::make_shared<FreeCameraMover>();

        _shader = std::make_shared<ShaderProgram>("597GrebenshchikovaData1/texture.vert",
                "597GrebenshchikovaData1/texture.frag");

        _worldTexture = loadTexture("597GrebenshchikovaData1/earth_global.jpg");
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        _marker = makeSphere(0.1f);
        _markerShader = std::make_shared<ShaderProgram>("597GrebenshchikovaData1/marker.vert", "597GrebenshchikovaData1/marker.frag");
    }

    void handleKey(int key, int scancode, int action, int mods) override {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS) {
                if (_detalization > 3) {
                    --_detalization;
                }
            }
            if (key == GLFW_KEY_EQUAL) {
                if (_detalization < 100) {
                    ++_detalization;
                }
            }
        }
    }
	
    void update() override
    {
        double dt = glfwGetTime() - _oldTime;
        _oldTime = glfwGetTime();

        _shift += 0.0003;
        _phi += 0.001;
        _theta += 0.001;
		
        _cameraMover->update(_window, dt);
        _camera = _cameraMover->cameraInfo();
        std::cout << "Detalization: " << _detalization << std::endl;

        _crossCapSurface = makeCrossCapSurface(1.0f, _detalization, _shift);
        _crossCapSurface->setModelMatrix(glm::mat4(1.0f));
    }
    
    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        
        glViewport(0, 0, width, height);
        
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();
        
        _shader->setFloatUniform("time", (float)glfwGetTime());
        
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace);
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        GLuint textureUnitForDiffuseTex = 0;

        glBindSampler(textureUnitForDiffuseTex, _sampler);
        glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
        _worldTexture->bind();

        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);
        _shader->setMat4Uniform("modelMatrix", _crossCapSurface->modelMatrix());

        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _crossCapSurface->modelMatrix()))));
        _crossCapSurface->draw();

        _markerShader->use();
        _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), -_light.position));
        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
        _marker->draw();

        glBindSampler(0, 0);
        glUseProgram(0);
    }
	
private:
    MeshPtr _crossCapSurface;
    unsigned int _detalization = 3;
    float _shift = 0;

    ShaderProgramPtr _markerShader;
    TexturePtr _worldTexture;
    GLuint _sampler;

    LightInfo _light;
    MeshPtr _marker;
    float _lr = 2.0;
    float _phi = 0.0;
    float _theta = 0.0;
};

int main()
{
    CrossCapSurfaceApplication app;
    app.start();
}