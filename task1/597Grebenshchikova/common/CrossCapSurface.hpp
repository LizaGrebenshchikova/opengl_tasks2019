#pragma once

#include "Mesh.hpp"
#include "Texture.hpp"

#include <iostream>
#include <vector>

MeshPtr makeCrossCapSurface(float C, unsigned int N, float shift)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
    
    for (unsigned int i = 0; i < N; ++i)
    {
        float theta = (float)glm::pi<float>() * i / N;
        float theta1 = (float)glm::pi<float>() * (i + 1) / N;
        
        for (unsigned int j = 0; j < N; ++j)
        {
	    	float phi = (float)glm::pi<float>() * j / N;
            float phi1 = (float)glm::pi<float>() * (j + 1) / N;

			vertices.push_back(glm::vec3(sin(theta) * sin(2 * phi) * C / 2, sin(2 * theta) * pow(cos(phi), 2) * C, cos(2 * theta) * pow(cos(phi), 2) * C));
			vertices.push_back(glm::vec3(sin(theta1) * sin(2 * phi) * C / 2, sin(2 * theta1) * pow(cos(phi), 2) * C, cos(2 * theta1) * pow(cos(phi), 2) * C));
			vertices.push_back(glm::vec3(sin(theta1) * sin(2 * phi1) * C / 2, sin(2 * theta1) * pow(cos(phi1), 2) * C, cos(2 * theta1) * pow(cos(phi1), 2) * C));


			normals.push_back(glm::vec3(
					-4 * pow(cos(phi), 3) * sin(phi),
					-2 * sin(2 * theta) * sin(theta) * pow(cos(phi), 2) * cos(2 * phi) + cos(2 * theta) * cos(theta) * sin(2 * phi) * cos(phi) * sin(phi),
					-cos(theta) * sin(2 * phi) * sin(2 * theta) * cos(phi) * sin(phi) - 2 * cos(2 * theta) * pow(cos(phi), 2) * sin(theta) * cos(2 * phi)
			));

			normals.push_back(glm::vec3(
					-4 * pow(cos(phi), 3) * sin(phi),
					-2 * sin(2 * theta1) * sin(theta1) * pow(cos(phi), 2) * cos(2 * phi) + cos(2 * theta1) * cos(theta1) * sin(2 * phi) * cos(phi) * sin(phi),
					-cos(theta1) * sin(2 * phi) * sin(2 * theta1) * cos(phi) * sin(phi) - 2 * cos(2 * theta1) * pow(cos(phi), 2) * sin(theta1) * cos(2 * phi)
			));

			normals.push_back(glm::vec3(
					-4 * pow(cos(phi1), 3) * sin(phi1),
					-2 * sin(2 * theta1) * sin(theta1) * pow(cos(phi1), 2) * cos(2 * phi1) + cos(2 * theta1) * cos(theta1) * sin(2 * phi1) * cos(phi1) * sin(phi1),
					-cos(theta1) * sin(2 * phi1) * sin(2 * theta1) * cos(phi1) * sin(phi1) - 2 * cos(2 * theta1) * pow(cos(phi1), 2) * sin(theta1) * cos(2 * phi1)
			));


			texCoords.push_back(glm::vec2(shift + theta / 2.0f / glm::pi<float>(), phi / 2. / glm::pi<float>()));
			texCoords.push_back(glm::vec2(shift + theta1 / 2.0f / glm::pi<float>(), phi / 2. / glm::pi<float>()));
			texCoords.push_back(glm::vec2(shift + theta1 / 2.0f / glm::pi<float>(), phi1 / 2. / glm::pi<float>()));

			vertices.push_back(glm::vec3(sin(theta) * sin(2 * phi) * C / 2, sin(2 * theta) * pow(cos(phi), 2) * C, cos(2 * theta) * pow(cos(phi), 2) * C));
			vertices.push_back(glm::vec3(sin(theta) * sin(2 * phi1) * C / 2, sin(2 * theta) * pow(cos(phi1), 2) * C, cos(2 * theta) * pow(cos(phi1), 2) * C));
			vertices.push_back(glm::vec3(sin(theta1) * sin(2 * phi1) * C / 2, sin(2 * theta1) * pow(cos(phi1), 2) * C, cos(2 * theta1) * pow(cos(phi1), 2) * C));


			normals.push_back(glm::vec3(
					-4 * pow(cos(phi), 3) * sin(phi),
					-2 * sin(2 * theta) * sin(theta) * pow(cos(phi), 2) * cos(2 * phi) + cos(2 * theta) * cos(theta) * sin(2 * phi) * cos(phi) * sin(phi),
					-cos(theta) * sin(2 * phi) * sin(2 * theta) * cos(phi) * sin(phi) - 2 * cos(2 * theta) * pow(cos(phi), 2) * sin(theta) * cos(2 * phi)
			));
			normals.push_back(glm::vec3(
					-4 * pow(cos(phi1), 3) * sin(phi1),
					-2 * sin(2 * theta) * sin(theta) * pow(cos(phi1), 2) * cos(2 * phi1) + cos(2 * theta) * cos(theta) * sin(2 * phi1) * cos(phi1) * sin(phi1),
					-cos(theta) * sin(2 * phi1) * sin(2 * theta) * cos(phi1) * sin(phi1) - 2 * cos(2 * theta) * pow(cos(phi1), 2) * sin(theta) * cos(2 * phi1)
			));
			normals.push_back(glm::vec3(
					-4 * pow(cos(phi1), 3) * sin(phi1),
					-2 * sin(2 * theta1) * sin(theta1) * pow(cos(phi1), 2) * cos(2 * phi1) + cos(2 * theta1) * cos(theta1) * sin(2 * phi1) * cos(phi1) * sin(phi1),
					-cos(theta1) * sin(2 * phi1) * sin(2 * theta1) * cos(phi1) * sin(phi1) - 2 * cos(2 * theta1) * pow(cos(phi1), 2) * sin(theta1) * cos(2 * phi1)
			));

			texCoords.push_back(glm::vec2(shift + theta / 2.0f / glm::pi<float>(), phi / 2. / glm::pi<float>()));
			texCoords.push_back(glm::vec2(shift + theta / 2.0f / glm::pi<float>(), phi1 / 2. / glm::pi<float>()));
			texCoords.push_back(glm::vec2(shift + theta1 / 2.0f / glm::pi<float>(), phi1 / 2. / glm::pi<float>()));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
    
    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

	DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
	buf2->setData(texCoords.size() * sizeof(float) * 2, texCoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
	mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount((GLint)vertices.size());
    
    return mesh;
}
